# README #

Welcome to the rt-n56u project

This project aims to improve the rt-n56u and other supported devices on the software part, allowing power user to take full control over their hardware.
This project was created in hope to be useful, but comes without warranty or support. Installing it will probably void your warranty. 
Contributors of this project are not responsible for what happens next.

### How do I get set up? ###

* [Get the tools to build the system](https://bitbucket.org/padavan/rt-n56u/wiki/EN/HowToMakeFirmware) or [Download pre-built system image](https://bitbucket.org/padavan/rt-n56u/downloads)
* Feed the device with the system image file (Follow instructions of updating your current system)
* Perform factory reset
* Open web browser on http://my.router to configure the services

### Contribution guidelines ###

* To be completed

I offer only the Base and an Advanced version for the RT-N65U Router. Both version are only english and german Frontend.
The Advanced version has also syslog and parted
.
[Download the pre-build system image for the RT-N65U self compiled with the following news and more:](https://bitbucket.org/dirkp78/update/downloads/)
feat(ipec-tools): add ipsec-tools 0.8.2
feat(dropbear): update to version 2019.78
feat(lighttpd): add lighttpd 1.4.53
feat(pcre): add pcre library 8.43
feat(openssl): update to version 1.0.2r
feat(ipsec): enable ipsec kernel modules
www: Advanced_Console_Content: fix 'ReferenceError: event is not defined'

NEW: RT-N65U Kernel 3.4 added

And RT-N56U 3.4.3.9L-100 english and german frontend. I've tested
[Changelog and more info](https://github.com/Linaro1985/padavan-ng/compare/v3.4.3.9-100L...master) 

Use at own risk. Benutzung auf eigenes Risiko